
## CMake Project based on TDAQ Release

This area shows an example of how to build a project based on the main TDAQ release.


The project organization should follow the structure outlined in the TDAQ CMake 
twiki

The top level directory shall contain a CMakeLists.txt file with the following
structure:

```
cmake_minimum_required(VERSION 3.14.0)
project(MyProject VERSION 1.0.0)
find_package(TDAQ)
tdaq_project(MyProject 1.0.0 USES tdaq 8.3.1)
```

Packages are directories below this which contain themselves a top level
CMakeLists.txt file.

The `MyRelease` packages shows how to generate a `setup.sh` file from
a template. Inside the template CMake variables can be referenced
with @MY_VARIABLE@. See the `configure_file()` documentation in CMake.

To allow users to check out a subset of package in a work area, create
the `cmake/MyProject.cmake.in` file (it is logically part of your top
level project structure, i.e. there is no CMakeLists.txt file here).

It should be named after your project, here `MyProject.cmake.in`.

The file will be run through through configure_file() and automatically
installed in the right place.

Build and install the project with `make install`.

Now create a WorkArea somewhere else, add a CMakeLists.txt file like:

```
cmake_minimum_required(VERSION 3.6.0)
project(work)
find_package(TDAQ)
tdaq_work_area(MyProject 1.0.0)
```

Before you start the build, add to the CMAKE_PREFIX_PATH variable:

```
export CMAKE_PREFIX_PATH=<path_to_my_project>/installed:${CMAKE_PREFIX_PATH}
```

If your project is installed following the TDAQ convention, i.e. like
```
${SOME_PATH}/MyProject/MyProject-01-00-00/installed
```

you can also specify:


```
export CMAKE_PREFIX_PATH=${SOME_PATH}:${CMAKE_PREFIX_PATH}
```

then the user can easily select another project version in his work area and
it will still be found.
